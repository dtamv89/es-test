<?php

namespace Tests\Unit;

use App\Services\ElasticSearchService;
use Cviebrock\LaravelElasticsearch\Manager as Elasticsearch;
use Tests\TestCase;

class ElasticSearchServiceTest extends TestCase
{
    /**
     * @var ElasticSearchService
     */
    protected $service;

    /**
     * @var Elasticsearch
     */
    protected $elasticsearch;

    /**
     *
     */
    protected function setUp()
    {
        $this->elasticsearch = \Mockery::mock(Elasticsearch::class);
        $this->service = new ElasticSearchService($this->elasticsearch);

    }

    /**
     * Test case when indexing fails
     */
    public function testIndexSingleRecordFailed()
    {
        $this->elasticsearch->shouldReceive('index')->once()->withAnyArgs()->andThrow(\Exception::class);

        $this->expectException(\Exception::class);
        $this->service->indexSingleRecord([]);
    }

    /**
     * Test case when indexing is successful
     */
    public function testIndexSingleRecordOk()
    {
        $this->elasticsearch->shouldReceive('index')->once()->withAnyArgs()->andReturn(true);

        $result = $this->service->indexSingleRecord([]);
        $this->assertEquals(null, $result);
    }

    /**
     * Test case when GetOneByIndexAndRecordId returns no result
     */
    public function testGetOneByIndexAndRecordIdNotFound()
    {
        $hits = [
            'hits' => [
                'hits' => []
            ]
        ];

        $this->elasticsearch->shouldReceive('search')->once()->withAnyArgs()->andReturn($hits);

        $result = $this->service->getOneByIndexAndRecordId('test', 1);
        $this->assertEquals(null, $result);
    }

    /**
     * Test case when GetOneByIndexAndRecordId returns a result
     */
    public function testGetOneByIndexAndRecordIdOk()
    {
        $hits = [
            'hits' => [
                'hits' => [
                    [
                        'id' => 1
                    ]
                ]
            ]
        ];

        $this->elasticsearch->shouldReceive('search')->once()->withAnyArgs()->andReturn($hits);

        $result = $this->service->getOneByIndexAndRecordId('test', 1);
        $this->assertIsArray($result);
        $this->assertEquals(1, $result['id']);
    }
}
