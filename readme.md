# Hulkshare Test


##Local Deploy

 - Install Docker
 - create `.env` file, you can just rename `.env.example` to `.env`
 - run `docker-composer up -d`
 - run `docker-compose exec php-fpm sh -c 'composer install'`
 - run `docker-compose exec php-fpm sh -c 'php artisan populate:db'` to populate DB
 

##Working with Elasticsearch
See section [Available Data Models](##available-data-models) to run commands properly.

 - run `docker-compose exec php-fpm sh -c 'php artisan index:data'` to index all data in ES
 - run `docker-compose exec php-fpm sh -c 'php artisan index:data {--dataModel=}'` to index all data
  of certain Model in ES, eg `docker-compose exec php-fpm sh -c 'php artisan index:data --dataModel=App\\Models\\Actor'`
 - run `docker-compose exec php-fpm sh -c 'php artisan index:data {--dataModel=} {--latestNumber=}'` 
 to index latest {n} records of certain Model in ES, eg `docker-compose exec php-fpm sh -c 'php artisan index:data --dataModel=App\\Models\\Actor --latestNumber=10'`
 - run `docker-compose exec php-fpm sh -c 'php artisan update:indexedData {dataModel} {ids}'` to update all data of 
 certain Model in ES where `{ids}` is a list of ids separated by `,`, eg `docker-compose exec php-fpm sh -c 'php 
 artisan update:indexedData App\\Models\\Actor 1,2,3'`

##Available Data Models
 - `App\\Models\\Actor`
 - `App\\Models\\Category`
 - `App\\Models\\Tag`
 - `App\\Models\\Video`
 
##Search Functionality
To make search use endpoint `http://localhost/api/search?searchValue=odio&size=100&from=0`. This endpoint supports 
following values:

 - `searchValue` - required
 - `size` - number of records to return (be default is 20)
 - `from` - return records starting from(aka `offset`)
 - for pagination you should use combination of `size` and `from` values

##Tests
I've added one Unit test for `ElasticSearchService`. To run it just execute `docker-compose exec php-fpm sh -c './vendor/bin/phpunit --filter=ElasticSearchServiceTest'`

## About Hulkshare Elasticsearch Test

As you have noticed in project we have Service layer that is responsible for business logic. If I have more time I 
would add some kind of Manager/Repository layer that is for working with DB data.

Having such layers allows us to have clear and flexible code. All code can be tested properly and independently.

For API routes Swagger ca be added

Also some code from Command could be moved to Service layer of appropriate resource in order to be re-used if we want
 to use that functionality in other places and not to repeat the code.