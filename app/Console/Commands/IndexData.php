<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Services\ElasticSearchService;
use App\Models\Actor;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Video;

/**
 * Class IndexData
 *
 * @package App\Console\Commands
 */
class IndexData extends Command
{
    /**
     * @var ElasticSearchService
     */
    protected $elasticSearchService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'index:data {--dataModel=} {--latestNumber=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Index data in Elasticsearch';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ElasticSearchService $elasticSearchService)
    {
        parent::__construct();

        $this->elasticSearchService = $elasticSearchService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dataModel = $this->option('dataModel');

        if (!empty($dataModel)) {
            $this->indexAllForModel($dataModel);
        } else {
            $this->indexAllData();
        }
    }

    /**
     *
     */
    protected function indexAllData()
    {
        // here we index all actors
        $actors = Actor::all();
        $bar = $this->output->createProgressBar($actors->count());
        $bar->start();

        foreach ($actors as $actor) {
            $data = [
                'body' => [
                    'id' => $actor->id,
                    'title' => $actor->title
                ],
                'index' => Actor::ES_INDEX,
                'type' => Actor::ES_INDEX
            ];

            $this->elasticSearchService->indexSingleRecord($data);
            $bar->advance();
        }

        $bar->finish();

        // here we index all categories
        $categories = Category::all();
        $bar = $this->output->createProgressBar($categories->count());
        $bar->start();

        foreach ($categories as $category) {
            $data = [
                'body' => [
                    'id' => $category->id,
                    'title' => $category->title
                ],
                'index' => Category::ES_INDEX,
                'type' => Category::ES_INDEX
            ];

            $this->elasticSearchService->indexSingleRecord($data);
            $bar->advance();
        }

        $bar->finish();

        // here we index all tags
        $tags = Tag::all();
        $bar = $this->output->createProgressBar($tags->count());
        $bar->start();

        foreach ($tags as $tag) {
            $data = [
                'body' => [
                    'id' => $tag->id,
                    'title' => $tag->title
                ],
                'index' => Tag::ES_INDEX,
                'type' => Tag::ES_INDEX
            ];

            $this->elasticSearchService->indexSingleRecord($data);
            $bar->advance();
        }

        $bar->finish();

        // here we index all videos
        $videos = Video::all();
        $bar = $this->output->createProgressBar($videos->count());
        $bar->start();

        foreach ($videos as $video) {
            $data = [
                'body' => [
                    'id' => $video->id,
                    'title' => $video->title,
                    'description' => $video->description,
                ],
                'index' => Video::ES_INDEX,
                'type' => Video::ES_INDEX
            ];

            $this->elasticSearchService->indexSingleRecord($data);
            $bar->advance();
        }

        $bar->finish();
    }

    /**
     * @param string $dataModel
     */
    protected function indexAllForModel(string $dataModel)
    {
        $latestNumber = $this->option('latestNumber');

        if (!empty($latestNumber)) {
            $records = $dataModel::orderBy('id', 'desc');
            $records->take($latestNumber);
            $records = $records->get();
        } else {
            $records = $dataModel::all();
        }

        $bar = $this->output->createProgressBar($records->count());
        $bar->start();

        foreach ($records as $record) {
            $data = [
                'body' => [
                    'id' => $record->id,
                    'title' => $record->title
                ],
                'index' => $record::ES_INDEX,
                'type' => $record::ES_INDEX
            ];

            if ($record instanceof Video) {
                $data['body']['description'] = $record->description;
            }

            $this->elasticSearchService->indexSingleRecord($data);
            $bar->advance();
        }

        $bar->finish();
    }
}
