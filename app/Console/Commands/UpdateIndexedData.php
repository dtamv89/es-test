<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Services\ElasticSearchService;
use App\Models\Actor;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Video;

/**
 * Class UpdateIndexedData
 *
 * @package App\Console\Commands
 */
class UpdateIndexedData extends Command
{
    /**
     * @var ElasticSearchService
     */
    protected $elasticSearchService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:indexedData {dataModel} {ids}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update already indexed data in Elasticsearch';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ElasticSearchService $elasticSearchService)
    {
        parent::__construct();

        $this->elasticSearchService = $elasticSearchService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dataModel = $this->argument('dataModel');
        $ids = $this->argument('ids');

        $records = $dataModel::whereIn('id', explode(',', $ids))->get();
        $bar = $this->output->createProgressBar($records->count());
        $bar->start();

        foreach ($records as $record) {
            $existingDocument = $this->elasticSearchService->getOneByIndexAndRecordId($record::ES_INDEX, $record->id);

            if ($existingDocument === null) {
                continue;
            }

            $data = [
                'body' => [
                    'doc' => [
                        'id' => $record->id,
                        'title' => $record->title
                    ]
                ],
                'index' => $record::ES_INDEX,
                'type' => $record::ES_INDEX,
                'id' => $existingDocument['_id']
            ];

            if ($record instanceof Video) {
                $data['body']['doc']['description'] = $record->description;
            }

            $this->elasticSearchService->updateDocument($data);
            $bar->advance();
        }

        $bar->finish();
    }
}
