<?php

namespace App\Services;

use App\Models\Actor;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Video;
use Cviebrock\LaravelElasticsearch\Manager as Elasticsearch;
use App;

/**
 * Class ElasticSearchService
 *
 * @package App\Services
 */
class ElasticSearchService
{
    public const DEFAULT_SIZE = 20;

    /**
     * @var Elasticsearch
     */
    protected $elasticsearch;

    /**
     * ElasticsearchService constructor.
     *
     * @param Elasticsearch $elasticsearch
     */
    public function __construct(Elasticsearch $elasticsearch)
    {
        $this->elasticsearch = $elasticsearch;
    }

    /**
     * This method is for adding new record to index
     *
     * @param array $data
     */
    public function indexSingleRecord(array $data): void
    {
        $this->elasticsearch->index($data);
    }

    /**
     * This method is for updating existing document in index
     *
     * @param array $data
     */
    public function updateDocument(array $data): void
    {
        $this->elasticsearch->update($data);
    }

    /**
     * This method is for fetching certain document from index
     *
     * @param string $index
     * @param int $id
     * @return null|array
     */
    public function getOneByIndexAndRecordId(string $index, int $id): ?array
    {
        $params = [
            'index' => $index,
            'type' => $index,
            'body' => [
                'query' => [
                    'match' => [
                        'id' => $id
                    ]
                ]
            ]
        ];

        $result = $this->elasticsearch->search($params);

        if (count($result['hits']['hits']) === 0) {
            return null;
        }

        return $result['hits']['hits'][0];
    }

    /**
     * This method is for making search
     *
     * @param string $searchValue
     * @param int|null $from
     * @param int|null $size
     * @return array
     */
    public function search(string $searchValue, ?int $from, ?int $size): array
    {
        $indices = implode(',', [
            Actor::ES_INDEX,
            Category::ES_INDEX,
            Tag::ES_INDEX,
            Video::ES_INDEX
        ]);

        $params = [
            'size' => $size ?: self::DEFAULT_SIZE,
            'from' => $from ?: 0,
            'index' => $indices,
            'type' => $indices,
            'search_type' => 'dfs_query_then_fetch',
            'body' => [
                'indices_boost' => [
                    Actor::ES_INDEX => 1.6,
                    Video::ES_INDEX => 1.4,
                    Tag::ES_INDEX => 1.2,
                    Category::ES_INDEX => 1,
                ],
                'query' => [
                    'bool' => [
                        'should' => [
                            [
                                'match' => [
                                    'title' => [
                                        'query' => $searchValue,
                                        'fuzziness' => '1',
                                        'boost' => 3
                                    ]
                                ]
                            ],
                            [
                                'match' => [
                                    'description' => [
                                        'query' => $searchValue,
                                        'boost' => 1
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $response = $this->elasticsearch->search($params);

        $countParams = $params;
        unset($countParams['size'], $countParams['from'], $countParams['search_type'], $countParams['body']['indices_boost']);
        $count = $this->elasticsearch->count($countParams);

        return [$response['hits']['hits'], $count['count']];
    }
}
