<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Actor extends Model
{
    public const ES_INDEX = 'actors';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'actors';
}