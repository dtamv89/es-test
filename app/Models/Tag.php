<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public const ES_INDEX = 'tags';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tags';
}