<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public const ES_INDEX = 'videos';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'videos';
}