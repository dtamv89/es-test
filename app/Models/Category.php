<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public const ES_INDEX = 'categories';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';
}