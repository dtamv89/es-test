<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\ElasticsearchService;

/**
 * Class SearchController
 *
 * @package App\Http\Controllers
 */
class SearchController extends Controller
{
    /**
     * @param ElasticsearchService $elasticearchService
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ElasticsearchService $elasticearchService, Request $request)
    {
        if (empty($request->query('searchValue'))) {
            return response()->json(['error' => '`searchValue` is required'], Response::HTTP_BAD_REQUEST);
        }

        [$result, $count] = $elasticearchService->search(
            $request->query('searchValue'),
            $request->query('from'),
            $request->query('size')
        );

        return response()->json(['data' => $result, 'count' => $count]);
    }
}